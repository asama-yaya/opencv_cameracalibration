#include "main.h"



int main(int argc, char *argv[]){

	/***********************************************************************/
	// capture
	/***********************************************************************/

	// 初期化
	webCamera.init(0);
	cv::Mat image = webCamera.capture();
	camera_width = image.cols;
	camera_height = image.rows;

	cv::Mat timg;
	cv::resize(image, timg, cv::Size(), 640.0 / (double)camera_width, 640.0 / (double)camera_width);
	cv::imshow("image", timg);
	cv::waitKey(10);

	cv::setMouseCallback("image", my_mouse_callback, (void *)&timg);

	// 撮影
	while (true){
		image = webCamera.capture();
		cv::resize(image, timg, cv::Size(), 640.0 / (double)camera_width, 640.0 / (double)camera_width);
		cv::imshow("image", timg);
		char key = cv::waitKey(10);

		// capture
		if (key == 'c' || captureFlag){
			cv::imwrite("data\\capture\\" + to_string(imgNum) + ".png", image);
			cout << "capture : " << imgNum << endl;
			imgNum++;
			captureFlag = false;
		}
		// next
		else if (key == 'q'){
			cv::destroyAllWindows();
			break;
		}
		// end
		else if (key == '\033'){
			cv::destroyAllWindows();
			return 0;
		}
	}

	/***********************************************************************/
	// image processing
	/***********************************************************************/

	// 撮影画像が０枚なら読み込み枚数を聞く
	while (imgNum == 0){
		cout << "What number of captured image? ";
		scanf_s("%d", &imgNum);
		getchar();
	}

	// 画像読み込み
	vector<cv::Mat> captureImages;
	for (int ii = 0; ii < imgNum; ii++){
		cv::Mat img = cv::imread("data\\capture\\" + to_string(ii) + ".png");
		if (img.cols < 10) break;
		captureImages.push_back(img);
	}
	imgNum = captureImages.size();
	cout << "The number of read image is: " << captureImages.size() << endl;

	// チェッカーを求める際の変数
	vector<vector<cv::Point2f>> imagePoints(imgNum);	// 撮影画像上の交点
	vector<vector<cv::Point3f>> worldPoints(imgNum);	// 対応する交点の三次元点
	vector<int> erasePoints;							// チェッカーを見つけられなかった画像

	cv::Size patternSize(checkX, checkY);

	// チェッカー探し
	for (int ii = 0; ii < imgNum; ii++){
		imagePoints[ii].clear();
		if (cv::findChessboardCorners(captureImages[ii], patternSize, imagePoints[ii], CV_CALIB_CB_ADAPTIVE_THRESH)){
			cout << "********* ";
			cout << "Success: " << ii << endl;

			// subpicel
			if (subPixelNeghtbor > 0){
				cv::Mat gray;
				cv::cvtColor(captureImages[ii], gray, CV_BGR2GRAY);
				cv::cornerSubPix(gray, imagePoints[ii], cv::Size(subPixelNeghtbor, subPixelNeghtbor), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 10000, 0.001));
			}

			// 検出点を描画する
			cv::drawChessboardCorners(captureImages[ii], patternSize, (cv::Mat)(imagePoints[ii]), true);
			cv::resize(captureImages[ii], timg, cv::Size(), 640.0 / (double)camera_width, 640.0 / (double)camera_width);
			cv::imshow("image", timg);
			cv::waitKey(10);

			cv::imwrite("data\\capture\\res_" + to_string(ii) + ".png", captureImages[ii]);
		}
		else{
			cout << "Fail: " << ii << endl;
			erasePoints.push_back(ii);
		}
	}
	if (erasePoints.size() == imgNum) return 0;

	// チェッカーパターンの交点の世界座標を決定する
	for (int ii = 0; ii < imgNum; ii++){
		worldPoints[ii].clear();
		for (int k = 0; k < patternSize.height; k++){
			for (int j = 0; j < patternSize.width; j++){
				worldPoints[ii].push_back(cv::Point3f(
					(float)(j * checkSize),
					(float)(k * checkSize),
					0.0f
					));
			}
		}
	}

	// 見つけられなかったのを削除
	for (int ii = erasePoints.size() - 1; ii >= 0; ii--){
		imagePoints.erase(imagePoints.begin() + erasePoints[ii]);
		worldPoints.erase(worldPoints.begin() + erasePoints[ii]);
	}
	imgNum -= erasePoints.size();

	/***********************************************************************/
	// calibration
	/***********************************************************************/

	// カメラパラメータ行列
	cv::Mat         cameraMatrix;       // 内部パラメータ行列
	cv::Mat         distCoeffs;         // レンズ歪み行列
	vector<cv::Mat> rotationVectors;    // 画像ごとに得られる回転ベクトル
	vector<cv::Mat> translationVectors; // 画像ごとに得られる平行移動ベクトル

	cout << "Start Calibration -> ";

	string xmlName = "data\\CAM_PARAM_MAT.xml";
	double calibError = 0;
	if (usePreviousRes){
		cv::FileStorage rfs(xmlName, cv::FileStorage::READ);
		bool bExistCameraParam = rfs.isOpened();
		calibError = cv::calibrateCamera(worldPoints, imagePoints, captureImages[0].size(),
			cameraMatrix, distCoeffs, rotationVectors, translationVectors);
	}
	else{
		calibError = cv::calibrateCamera(worldPoints, imagePoints, captureImages[0].size(),
			cameraMatrix, distCoeffs, rotationVectors, translationVectors);
	}
	cout << "error = " << calibError << endl;


	//カメラ内部パラメータの計算
	double apertureWidth = 0, apertureHeight = 0;  // センサの物理的な幅・高さ
	double fovx = 0, fovy = 0;                                              // 出力される水平（垂直）軸にそった画角（度単位）
	double focalLength = 0;                                                 // mm単位で表されたレンズの焦点距離
	cv::Point2d principalPoint = (0, 0);									// ピクセル単位で表されたレンズの焦点距離
	double aspectRatio = 0;                                                 // アスペクト比

	cout << "Calc Camera Param -> ";
	cv::calibrationMatrixValues(cameraMatrix, captureImages[0].size(), apertureWidth, apertureHeight,
		fovx, fovy, focalLength, principalPoint, aspectRatio);

	// XMLファイルへ結果を出力する
	cout << "Start Output Xml File -> ";

	cv::FileStorage wfs(xmlName, cv::FileStorage::WRITE);
	wfs << "CameraMatrix" << cameraMatrix;
	wfs << "DistortCoeffs" << distCoeffs;

	wfs << "aperture_Width" << apertureWidth;
	wfs << "aperture_Height" << apertureHeight;
	wfs << "fov_x" << fovx;
	wfs << "fov_y" << fovy;
	wfs << "focal_Length" << focalLength;
	wfs << "principal_Point" << principalPoint;
	wfs << "aspect_Ratio" << aspectRatio;
	wfs << "calibration_error" << calibError;
	wfs.release();

	cout << "Finish Output Xml File" <<endl;

	getchar();
	return 0;
}


// コールバック関数
void my_mouse_callback(int event, int x, int y, int flags, void* param){
	cv::Mat* image = static_cast<cv::Mat*>(param);

	switch (event){

	case cv::EVENT_RBUTTONDOWN:
		captureFlag = true;
		break;
	default:
		break;
	}
}





















