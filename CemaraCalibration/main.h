#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Windows.h>
#include <math.h>
#include <time.h>

using std::cout;
using std::endl;

using std::vector;
using std::string;
using std::to_string;
using std::stoi;
using std::stod;
using std::stoul;

using std::ifstream;
using std::ofstream;
using std::ios;

// OpenCV---------------------------------------------------------------------------------------
#include <opencv2/opencv.hpp>		// インクルードファイル指定 
#include <opencv2/opencv_lib.hpp>	// 静的リンクライブラリの指定
// MyHeader-------------------------------------------------------------------------------------
#include "camera_class.h"


TWebCamera webCamera;

// 入力が必要かもしれない変数
int camera_width;
int camera_height;

int imgNum = 0;
double checkSize = 19.0 / 1.0;
int checkX = 7;
int checkY = 10;

int subPixelNeghtbor = 3;

bool usePreviousRes = false;

// どうでもいい変数
bool captureFlag = false;

void my_mouse_callback(int event, int x, int y, int flags, void* param);